module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          'pouchdb-collate': '@craftzdog/pouchdb-collate-react-native',
        },
      },
    ],
  ],
};
