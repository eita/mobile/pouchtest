/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {shim} from 'react-native-quick-base64';

shim();

// Avoid using node dependent modules
process.browser = true;

AppRegistry.registerComponent(appName, () => App);
